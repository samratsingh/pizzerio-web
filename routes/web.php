<?php
  
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/password-changed-success', function () {
    return view('password_changed_success');
});

/* Laravel Default Authentication Routes */

Auth::routes();

/* User Routes */
Route::get('/', 'OwnerController@dashboard')->name('dashboard');
Route::get('/employee-performance', 'OwnerController@employeePerformance')->name('employee-performance');
Route::get('/user/logout', 'Auth\LoginController@userLogout')->name('user-logout');
Route::get('/owner-profile', 'AdminController@showOwnerProfile')->name('view-owner-profile');
Route::get('/edit-owner-profile', 'AdminController@showOwnerProfileForm')->name('edit-owner-profile');
Route::post('/submit-owner-profile', 'AdminController@submitOwnerProfile')->name('submit-owner-profile');

Route::get('/owner-change-password', 'AdminController@showChangePasswordForm')->name('owner-change-password');
Route::post('/submit-owner-password', 'AdminController@submitOwnerPassword')->name('submit-owner-password');


Route::group(['prefix' => '/admin', 'middleware' => 'role.admin'], function(){
	
	/* Hotel Owner Controller */
	Route::get('/select-hotel', 'AdminController@showSelectHotel')->name('select-hotel');
	Route::get('/select-hotel-submit{hotel_id}', 'AdminController@submitSelectHotel')->name('select-hotel-submit');
	Route::get('/view-owners', 'AdminController@viewOwners')->name('view-owners');
	Route::get('/edit-owner/{owner_id}', 'AdminController@editOwner')->name('edit-owner');
	Route::post('/submit-edit-owner', 'AdminController@submitEditOwner')->name('submit-edit-owner');
	Route::get('/add-new-owner', 'AdminController@showAddNewOwnerForm')->name('add-new-owner');
	Route::post('/submit-add-new-owner', 'AdminController@submitAddNewOwner')->name('submit-add-new-owner');
	Route::get('/hotel-details', 'AdminController@showHotelDetails')->name('view-hotel-details');
	Route::get('/enter-hotel-details', 'AdminController@showHotelDetailsForm')->name('enter-hotel-details');
	Route::get('/edit-hotel-details', 'AdminController@showHotelDetailsForm')->name('edit-hotel-details');
	Route::post('/submit-hotel-details', 'AdminController@submitHotelDetails')->name('submit-hotel-details');
	Route::post('/manage-menus', 'AdminController@manageMenus')->name('manage-menus');
	
	/* Menus Controller */
	/*Route::get('/manage-menus', 'MenusController@manageMenus')->name('manage-menus');
	Route::get('/create-new-menu', 'MenusController@showCreateMenuForm')->name('create-new-menu');
	Route::post('/submit-new-menu', 'MenusController@createNewMenu')->name('submit-new-menu');
	Route::get('/delete-menu/{menu_id}', 'MenusController@deleteMenu')->name('delete-menu');
	Route::get('/edit-menu/{menu_id}', 'MenusController@editMenu')->name('edit-menu');
	Route::post('/submit-menu', 'MenusController@submitMenu')->name('submit-menu');
	*/
	Route::get('/add-food-item', 'MenusController@showFoodItemForm')->name('add-food-item');
	Route::get('/manage-food-items', 'MenusController@manageFoodItems')->name('manage-food-items');
	Route::get('/edit-food-item/{food_item_id}', 'MenusController@editFoodItem')->name('edit-food-item');
	Route::post('/edit-food-item', 'MenusController@submitEditFoodItem')->name('submit-edit-food-item');
	Route::post('/submit-food-item', 'MenusController@submitFoodItem')->name('submit-food-item');
	Route::get('/delete-food-item{food_item_id}', 'MenusController@deleteFoodItem')->name('delete-food-item');
	Route::post('/change-food-item-status', 'MenusController@changeFoodItemStatus')->name('change-food-item-status');
	Route::get('/link-food-items', 'MenusController@linkFoodItems')->name('link-food-items');
	Route::post('/link-food-items', 'MenusController@linkFoodItemsSubmit')->name('link-food-items-submit');
	Route::post('/get-linked-menus', 'MenusController@getLinkedMenus')->name('get-linked-menus');
	Route::get('/add-food-ingredient', 'MenusController@showFoodIngredientForm')->name('add-food-ingredient');
	Route::post('/submit-food-ingredient', 'MenusController@submitFoodIngredient')->name('submit-food-ingredient');
	Route::get('/manage-food-ingredients', 'MenusController@manageFoodIngredients')->name('manage-food-ingredients');
	Route::get('/edit-food-ingredient/{food_ingredient_id}', 'MenusController@editFoodIngredient')->name('edit-food-ingredient');
	Route::post('/edit-food-ingredient', 'MenusController@submitEditFoodIngredient')->name('submit-edit-food-ingredient');
	Route::post('/change-food-ingredient-status', 'MenusController@changeFoodIngredientStatus')->name('change-food-ingredient-status');
	Route::get('/delete-food-ingredient{food_ingredient_id}', 'MenusController@deleteFoodIngredient')->name('delete-food-ingredient');
	
	Route::get('/create-new-device', 'AdminController@showCreateDeviceForm')->name('create-new-device');
	Route::post('/submit-new-device', 'AdminController@createNewDevice')->name('submit-new-device');
	Route::get('/manage-device', 'AdminController@manageDevices')->name('manage-device');
	Route::get('/delete-device/{device_id}', 'AdminController@deleteDevice')->name('delete-device');
	Route::get('/edit-device/{device_id}', 'AdminController@editDevice')->name('edit-device');
	Route::post('/submit-device', 'AdminController@submitDevice')->name('submit-device');
	Route::get('/manage-categories', 'MenusController@manageCategories')->name('manage-categories');
	Route::get('/create-new-category', 'MenusController@showCreateCategoryForm')->name('create-new-category');
	Route::post('/submit-new-category', 'MenusController@createNewCategory')->name('submit-new-category');
	Route::post('/change-category-status', 'MenusController@changeCategoryStatus')->name('change-category-status');
	Route::get('/edit-category/{food_category_id}', 'MenusController@editFoodCategory')->name('edit-category');
	Route::post('/submit-edit-category', 'MenusController@submitEditFoodCategory')->name('submit-edit-category');
	Route::get('/delete-food-category{food_item_id}', 'MenusController@deleteFoodCategory')->name('delete-food-category');
	
	Route::get('/manage-employees', 'AdminController@manageEmployees')->name('manage-employees');
	Route::get('/add-employee', 'AdminController@showCreateEmployeeForm')->name('add-employee');
	Route::post('/submit-add-employee', 'AdminController@submitAddEmployee')->name('submit-add-employee');
	Route::get('/edit-employee/{employee_id}', 'AdminController@editEmployee')->name('edit-employee');
	Route::post('/submit-edit-employee', 'AdminController@submitEditEmployee')->name('submit-edit-employee');
	Route::get('/delete-employee/{employee_id}', 'AdminController@deleteEmployee')->name('delete-employee');
	
	// Delete must be delete request type.
	// Improve controller method naming convention, always think about conventions before starting project
});

