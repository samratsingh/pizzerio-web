<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\UserDeviceInfo;
use App\FirebaseNotification;
class FirebaseController extends Controller
{
    
    /**
     * Send Notification
     *
     * @return [json] user object
     */
    public function SendNotification(Request $request) {
		$userIds = User::where('hotel_id', $request->user()->hotel_id)->where('id','!=' , $request->user()->id)->pluck('id');
		//$deviceTokens = UserDeviceInfo::whereIn('user_id', $userIds)->pluck('device_token');
		$deviceTokens = array("dsa3uuJS4gI:APA91bGOg0WdRCGpCyoFKXC9A8-fIAF1Bg-Fc-CqgyaYbl1prfBdLqJPqYI7WMF7dOMR-MxNAorex8WVO8CvmJ9rK_BZM-vWTyCGxFTcOuK0sMjO0ufZIqqKfcUqGfbQkfPmUbJOosYm");
		$message = "hi";
		$title = "Order Confirmed";
		$result = json_decode(FirebaseNotification::instance()->sendNotification($title, $message, $deviceTokens));
		
		if($result->success){
			echo $result->success;
		}
		return json_encode($result);
	}
}