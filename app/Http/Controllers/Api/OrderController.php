<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\FoodItem;
use App\OrderDetail;
use App\FoodCategory;
use App\OrderSummary;
use App\UserDeviceInfo;
use App\FirebaseNotification;
use App\Api\Models\AssignEmployee;
use App\Employee;
class OrderController extends Controller
{
    
    /**
     * Add To Cart Items
     *
     * @return [json] user object
     */
    public function AddToCart(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'submenu_id'		=> 'required|integer',
            'item_count'		=> 'required|integer',
            'token_id' 			=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$orderSummary = $request->token_id == 0 ? new OrderSummary() : OrderSummary::find($request->token_id);
			$orderSummary->user_id = $request->user()->id;
			$orderSummary->hotel_id = $request->user()->hotel_id;
			$orderSummary->save();
			$foodItem = FoodItem::find($request->submenu_id);
			$itemAddStatus = false;
			if($foodItem != null && ($foodItem->status == 1)){
				$foodCategory = FoodCategory::find($foodItem->category_id);
				if($foodCategory != null && ($foodCategory->status == 1)){
					$orderDetail = new OrderDetail();
					$orderDetail->user_id = $request->user()->id;
					$orderDetail->item_id = $request->submenu_id;
					$orderDetail->item_count = $request->item_count;
					$orderDetail->price = $foodItem->price;
					$orderDetail->discount = $foodItem->discount;
					$orderDetail->summary_id = $orderSummary->id;
					$orderDetail->save();
					if($request->extra_ingredients != null){
						$orderDetail->extraIngredients()->sync(explode(',', $request->extra_ingredients));
					}
					$itemAddStatus = true;
				}
			}
			if($itemAddStatus){
				$orderDetails = $orderSummary->orderDetails;
				$response = [
								'message' 		=> 'Successfully Added To Cart',
								'code'			=> 200,
								'status' 		=> true,
								'order_details'	=> $orderDetails,
								'token_id'		=> $orderSummary->id,
								'total_items'	=> $orderDetails->count(),
							];
			}else {
				$orderDetails = $orderSummary->orderDetails;
				$response = [
								'message' 		=> 'Out of stock',
								'code'			=> 500,
								'status' 		=> false,
								'order_details'	=> $orderDetails,
								'token_id'		=> $orderSummary->id,
								'total_items'	=> $orderDetails->count(),
							];
			}
		}
        return json_encode($response);
    }
	
	/**
     * view cart summary
     *
     * @return [json] user object
     */
    public function ViewSummary(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'token_id' 		=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$orderSummary = OrderSummary::find($request->token_id);
			if ($orderSummary == null) {
				$response = [
								'message' 		=> 'List Not Found',
								'code'			=> 404,
								'status' 		=> false,
								'order_details'	=> array(),
								'token_id'		=> 0
							];
			}else{
				$orderDetails = $orderSummary->orderDetails;
				$orderDetailsResponse = array();
				$grand_total = 0;
				foreach($orderDetails as $orderDetail){
					$foodItem = FoodItem::find($orderDetail->item_id);
					$submenuName = $foodItem->name ?? "";
					$submenuImage = asset($foodItem->image ?? "");
					$submenuStatus = 0;
					if($foodItem != null && ($foodItem->status == 1)){
						$foodCategory = FoodCategory::find($foodItem->category_id);
						if($foodCategory != null && ($foodCategory->status == 1)){
							$submenuStatus = $foodItem->status;
							$grand_total += $orderDetail->price*$orderDetail->item_count;
						}
					}
					array_push($orderDetailsResponse, array(
													'id'			=> $orderDetail->id,
													'submenu_id'	=> $orderDetail->item_id,
													'submenu_name'	=> $submenuName,
													'submenu_image'	=> $submenuImage,
													'submenu_status'=> $submenuStatus,
													'item_count'	=> $orderDetail->item_count,
													'item_price'	=> "".$orderDetail->price,
													'total_price'	=> "".$orderDetail->price*$orderDetail->item_count,
													'discount'		=> "".$orderDetail->discount,
													'extra_ingredients'=> $orderDetail->extraIngredients,
													));
				}
				$response = [
								'message' 		=> 'List Fetched Successfully',
								'code'			=> 200,
								'status' 		=> true,
								'order_details'	=> $orderDetailsResponse,
								'token_id'		=> $orderSummary->id,
								'total_items'	=> $orderDetails->count(),
								'grand_total'	=> "".$grand_total,
							];
			}
		}
        return json_encode($response);
    }
	/**
     * Delete item from cart
     *
     * @return [json] user object
     */
    public function DeleteFromCart(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'token_id' 		=> 'required|integer',
            'detail_id' 	=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$deleteResult = false;
			if($request->detail_id == 0){
				$deleteResult = OrderDetail::where('summary_id',$request->token_id)->delete();
			}else {
				$deleteResult = OrderDetail::where('summary_id',$request->token_id)->where('id',$request->detail_id)->delete();
			}
			if($deleteResult) {
				$orderSummary = OrderSummary::find($request->token_id);
				$orderDetails = $orderSummary->orderDetails;
				$orderDetailsResponse = array();
				$grand_total = 0;
				foreach($orderDetails as $orderDetail){
					$foodItem = FoodItem::find($orderDetail->item_id);
					$submenuName = $foodItem->name ?? "";
					$submenuImage = asset($foodItem->image ?? "");
					$submenuStatus = 0;
					if($foodItem != null && ($foodItem->status == 1)){
						$foodCategory = FoodCategory::find($foodItem->category_id);
						if($foodCategory != null && ($foodCategory->status == 1)){
							$submenuStatus = $foodItem->status;
							$grand_total += $orderDetail->price*$orderDetail->item_count;
						}
					}
					array_push($orderDetailsResponse, array(
													'id'			=> $orderDetail->id,
													'submenu_id'	=> $orderDetail->item_id,
													'submenu_name'	=> $submenuName,
													'submenu_image'	=> $submenuImage,
													'submenu_status'=> $submenuStatus,
													'item_count'	=> $orderDetail->item_count,
													'item_price'	=> "".$orderDetail->price,
													'total_price'	=> "".$orderDetail->price*$orderDetail->item_count,
													'discount'		=> "".$orderDetail->discount,
													'extra_ingredients'=> $orderDetail->extraIngredients,
													));
				}
				$response = [
								'message' 		=> 'List Fetched Successfully',
								'code'			=> 200,
								'status' 		=> true,
								'order_details'	=> $orderDetailsResponse,
								'token_id'		=> $orderSummary->id,
								'total_items'	=> $orderDetails->count(),
								'grand_total'	=> "".$grand_total,
							];
			}
			else {
				$response = [
							'message' 		=> 'Item Not Found',
							'code'			=> 404,
							'total_items'	=> OrderDetail::where('summary_id',$request->token_id)->count(),
							'status' 		=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * increment/decrement item_count item from cart
     *
     * @return [json] user object
     */
    public function incDecItemCount(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'token_id' 		=> 'required|integer',
            'detail_id' 	=> 'required|integer',
			'inc_dec_type'	=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$updateResult = false;
			$orderDetail = OrderDetail::where('summary_id',$request->token_id)->where('id',$request->detail_id)->first();
			if($orderDetail != null){
				if($request->inc_dec_type == 0){
					if($orderDetail->item_count == 1) {
						//$updateResult = $orderDetail->delete();
					} else {
						$orderDetail->item_count = $orderDetail->item_count-1;
						$updateResult = $orderDetail->save();
					}
				}else {
					$orderDetail->item_count = $orderDetail->item_count+1;
					$updateResult = $orderDetail->save();
				}
			}
			if($updateResult) {
				$orderSummary = OrderSummary::find($request->token_id);
				$orderDetails = $orderSummary->orderDetails;
				$orderDetailsResponse = array();
				$grand_total = 0;
				foreach($orderDetails as $orderDetail){
					$foodItem = FoodItem::find($orderDetail->item_id);
					$submenuName = $foodItem->name ?? "";
					$submenuImage = asset($foodItem->image ?? "");
					$submenuStatus = 0;
					if($foodItem != null && ($foodItem->status == 1)){
						$foodCategory = FoodCategory::find($foodItem->category_id);
						if($foodCategory != null && ($foodCategory->status == 1)){
							$submenuStatus = $foodItem->status;
							$grand_total += $orderDetail->price*$orderDetail->item_count;
						}
					}
					array_push($orderDetailsResponse, array(
													'id'			=> $orderDetail->id,
													'submenu_id'	=> $orderDetail->item_id,
													'submenu_name'	=> $submenuName,
													'submenu_image'	=> $submenuImage,
													'submenu_status'=> $submenuStatus,
													'item_count'	=> $orderDetail->item_count,
													'item_price'	=> "".$orderDetail->price,
													'total_price'	=> "".$orderDetail->price*$orderDetail->item_count,
													'discount'		=> "".$orderDetail->discount,
													'extra_ingredients'=> $orderDetail->extraIngredients,
													));
				}
				$response = [
								'message' 		=> 'List Fetched Successfully',
								'code'			=> 200,
								'status' 		=> true,
								'order_details'	=> $orderDetailsResponse,
								'token_id'		=> $orderSummary->id,
								'total_items'	=> $orderDetails->count(),
								'grand_total'	=> "".$grand_total,
							];
			}
			else {
				$response = [
							'message' 		=> 'Item Not Found',
							'code'			=> 404,
							'total_items'	=> OrderDetail::where('summary_id',$request->token_id)->count(),
							'token_id'		=> $request->token_id,
							'status' 		=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * Confirm Order
     *
     * @return [json] user object
     */
    public function ConfirmOrder(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'items_array'	=> 'required|string',
            'token_id' 		=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$orderSummary = OrderSummary::find($request->token_id);
			$orderDetailIds = $orderSummary->orderDetails->pluck('item_id');
			$UnpublishedfoodItem = FoodItem::whereIn('id', $orderDetailIds)->where('status', 0)->first();
			if($UnpublishedfoodItem == null && OrderDetail::where('summary_id', $orderSummary->id)->sum('price') != 0){
				$orderDetails = json_decode($request->items_array);

				foreach($orderDetails as $key => $value) {
					OrderDetail::where('id', $value->item_id)->update(['item_count' => $value->item_count]);
				}
				
				$latestSummaryToken = OrderSummary::where('hotel_id', $request->user()->hotel_id)->whereDate('updated_at', Carbon::today())->max('daily_token');
				$dailyToken = $latestSummaryToken != null ? $latestSummaryToken+1 : 1;
				
				$latestSummaryOrder = OrderSummary::where('hotel_id', $request->user()->hotel_id)->max('order_no');
				$orderNo = $latestSummaryOrder != null ? $latestSummaryOrder+1 : 1000;
				
				if($orderSummary != null){
					$totalPrice = 0;
					$orderDetails = OrderDetail::where('summary_id', $orderSummary->id)->get();
					foreach($orderDetails as $orderDetail){
						$totalPrice += $orderDetail->price*$orderDetail->item_count;
					}
					$totalDiscount = OrderDetail::where('summary_id', $orderSummary->id)->sum('discount');
					$orderSummary->daily_token = $dailyToken;
					$orderSummary->order_no = $orderNo;
					$orderSummary->note = $request->note != null ? $request->note : "";
					$orderSummary->total_price = $totalPrice;
					$orderSummary->total_discount = $totalDiscount;
					$orderSummary->status = 1;
					$orderSummary->save();
						
					$userIds = User::where('hotel_id', $request->user()->hotel_id)->where('id','!=' , $request->user()->id)->where('role', 3)->pluck('id');
					$deviceTokens = UserDeviceInfo::whereIn('user_id', $userIds)->pluck('device_token');
					$message = "Please Refresh List";
					$title = "Order Confirmed";
					
					$responseMessage = "Something went wrong to send Notification to kitchen";
					if(!empty($deviceTokens)){
						$result = json_decode(FirebaseNotification::instance()->sendNotification($title, $message, $deviceTokens));
						
						if($result != null && $result->success){
							$responseMessage = "Successfully Confirmed";
						}
					}	
					$response = [
									'message' 		=> $responseMessage,
									'code'			=> 200,
									'status' 		=> true,
									'token'			=> $dailyToken,
									'token_id'		=> 0,
								];
				}
				else {
					$response = [
									'message' 		=> 'Token not found',
									'code'			=> 404,
									'status' 		=> false,
									'token'			=> 0,
									'token_id'		=> $request->token_id+0,
								];
				}
			}else {
				$response = [
									'message' 		=> $UnpublishedfoodItem != null ? $UnpublishedfoodItem->name.' is out of stock please remove from your cart': 'Please add item in your cart',
									'code'			=> 404,
									'status' 		=> false,
									'token'			=> 0,
									'token_id'		=> $request->token_id+0,
								];
			}
			
		}
        return json_encode($response);
    }
	
	/**
     * view orders
     *
     * @return [json] user object
     */
    public function ViewOrders(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'token_id' 			=> 'required|integer',
			'token_status'		=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
							'data'		=> array(),
						];
		}else{
			$status = true;
			$responseCode = 200;
			$message = 'List Fetched Successfully';
			if($request->token_id != 0){
				$orderDetail = OrderDetail::where('summary_id', $request->token_id)->first();
				if($orderDetail->prepared_by != 0){
					OrderDetail::where('summary_id', $request->token_id)->update(['prepare_end_time' => Carbon::now()->toDateTimeString()]);
					OrderSummary::where('id', $request->token_id)->update(['status' => 2]);
				}else{
					$status = false;
					$responseCode = 200;
					$message = 'Order not picked by anyone';
				}
			}
			$orderSummaries = OrderSummary::where('status',$request->token_status)->where('hotel_id', $request->user()->hotel_id)->get();
			if($request->token_status == 2){
				$orderSummaries = OrderSummary::where('status',$request->token_status)->where('hotel_id', $request->user()->hotel_id)->orderBy('id', 'DESC')->get();
			}
			if ($orderSummaries->isEmpty()) {
				$response = [
								'message'	=> 'List Not Found',
								'code'		=> 404,
								'status' 	=> false,
								'data'		=> array(),
							];
			}else{
				$orderSummariesResponse = array();
				foreach($orderSummaries as $orderSummary){
					$orderDetails = $orderSummary->orderDetails;
					$orderDetailsResponse = array();
					$employeeName = "";
					$preparationTime = "";
					foreach($orderDetails as $orderDetail){
						$foodItem = FoodItem::find($orderDetail->item_id);
						$employee = Employee::find($orderDetail->prepared_by);
						if($orderSummary->status == 2){
							$startTime = Carbon::parse($orderDetail->prepare_start_time);
							$endTime = Carbon::parse($orderDetail->prepare_end_time);
							
							$preparationTime =  gmdate('H:i:s', $startTime->diffInSeconds($endTime));
						}
						$employeeName = $employee->name ?? "";
						array_push($orderDetailsResponse, array(
														'id'			=> $orderDetail->id,
														'submenu_id'	=> $orderDetail->item_id,
														'submenu_name'	=> $foodItem->name ?? "",
														'submenu_image'	=> asset($foodItem->image ?? ""),
														'item_count'	=> $orderDetail->item_count,
														'item_price'	=> "".$orderDetail->price,
														'total_price'	=> "".$orderDetail->price*$orderDetail->item_count,
														'discount'		=> "".$orderDetail->discount,
														'extra_ingredients'=> $orderDetail->extraIngredients,
														));
					}
					array_push($orderSummariesResponse, array(
								'token_id'		=> $orderSummary->id,
								'token_no'		=> $orderSummary->daily_token,
								'order_no'		=> $orderSummary->order_no,
								'note'			=> $orderSummary->note,
								'order_date'	=> $orderSummary->updated_at->format('Y-m-d h:m:s'),
								'prepared_by'	=> $employeeName,
								'preparation_time'	=> $preparationTime,
								'order_details' => $orderDetailsResponse,
							));
				}
				$response = [
								'message' 		=> $message,
								'code'			=> $responseCode,
								'status' 		=> $status,
								'data'	=> $orderSummariesResponse,
							];
			}
		}
        return json_encode($response);
    }
	/**
     * remove extraIngredient
     *
     * @return [json] user object
     */
    public function RemoveExtraIngredient(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'token_id' 		=> 'required|integer',
            'detail_id' 	=> 'required|integer',
			'ingredient_id'	=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$updateResult = false;
			$orderDetail = OrderDetail::where('summary_id',$request->token_id)->where('id',$request->detail_id)->first();
			if($orderDetail != null){
					$updateResult = $orderDetail->extraIngredients()->detach($request->ingredient_id);
			}
			if($updateResult) {
				$orderSummary = OrderSummary::find($request->token_id);
				$orderDetails = $orderSummary->orderDetails;
				$orderDetailsResponse = array();
				$grand_total = 0;
				foreach($orderDetails as $orderDetail){
					$foodItem = FoodItem::find($orderDetail->item_id);
					$submenuName = $foodItem->name ?? "";
					$submenuImage = asset($foodItem->image ?? "");
					$submenuStatus = 0;
					if($foodItem != null && ($foodItem->status == 1)){
						$foodCategory = FoodCategory::find($foodItem->category_id);
						if($foodCategory != null && ($foodCategory->status == 1)){
							$submenuStatus = $foodItem->status;
							$grand_total += $orderDetail->price*$orderDetail->item_count;
						}
					}
					array_push($orderDetailsResponse, array(
													'id'			=> $orderDetail->id,
													'submenu_id'	=> $orderDetail->item_id,
													'submenu_name'	=> $submenuName,
													'submenu_image'	=> $submenuImage,
													'submenu_status'=> $submenuStatus,
													'item_count'	=> $orderDetail->item_count,
													'item_price'	=> "".$orderDetail->price,
													'total_price'	=> "".$orderDetail->price*$orderDetail->item_count,
													'discount'		=> "".$orderDetail->discount,
													'extra_ingredients'=> $orderDetail->extraIngredients,
													));
				}
				$response = [
								'message' 		=> 'List Fetched Successfully',
								'code'			=> 200,
								'status' 		=> true,
								'order_details'	=> $orderDetailsResponse,
								'token_id'		=> $orderSummary->id,
								'total_items'	=> $orderDetails->count(),
								'grand_total'	=> "".$grand_total,
							];
			}
			else {
				$response = [
							'message' 		=> 'Item Not Found',
							'code'			=> 404,
							'total_items'	=> OrderDetail::where('summary_id',$request->token_id)->count(),
							'token_id'		=> $request->token_id,
							'status' 		=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * Get All Employees
     *
     * @return [json] FoodItem object
     */
    public function assignEmployee(Request $request)
    {
		$assignEmployee = new AssignEmployee();
        $validator = Validator::make($request->all(),[
            'token_id' 		=> 'required|integer',
            'employee_id' 	=> 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			
			$assignEmployee->setMessage($errorMessage);
			$assignEmployee->setResponseCode(500);
			$assignEmployee->setStatus(false);
		}else{
			$updateResult = OrderDetail::where('summary_id', $request->token_id)->update(['prepared_by' => $request->employee_id, 'prepare_start_time' => Carbon::now()->toDateTimeString(), 'prepare_end_time' => Carbon::now()->toDateTimeString()]);
			
			if($updateResult) {
				$assignEmployee->setMessage("Assigned Successfully");
				$assignEmployee->setResponseCode(200);
				$assignEmployee->setStatus(true);
			}
		}
        return $assignEmployee->getJson();
    }
}