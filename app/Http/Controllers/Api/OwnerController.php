<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Carbon\Carbon;
use App\User;
use App\Hotel;
use App\FoodType;
use App\FoodItem;
use App\OrderDetail;
use App\OrderSummary;
use App\Employee;
use App\Api\Models\GetHotels;
use App\Api\Models\SelectHotel;
use App\Api\Models\UserLogin;
use Config;
class OwnerController extends Controller
{
    
    /**
     * create device
     *
     * @return [json] object
     */
    public function CreateDevice(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'password' => 'required|string|confirmed',
            'role' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$user = User::where('email', $request->name."_".$request->role.$request->user()->hotel_id)->first();
			if($user == null){
				$user = new User();
				$user->name = $request->name;
				$user->email = $request->name."_".$request->role.$request->user()->hotel_id;
				$user->hotel_id = $request->user()->hotel_id;
				$user->role = $request->role;
				$user->password = bcrypt($request->password);
				$user->save();
				$response = [
								'message' 	=> 'Successfully created device!',
								'code'		=> 200,
								'status' 	=> true,
								'device'	=> $user,
							];
			}else {
				$response = [
								'message' 	=> 'Name Already Exist',
								'code'		=> 400,
								'status' 	=> false,
							];
			}
		}
        return json_encode($response);
    }
	/**
     * update device
     *
     * @return [json] object
     */
    public function UpdateDevice(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
            'name' => 'required|string',
            'password' => 'confirmed',
            'role' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$user = User::find($request->id);
			$user->name = $request->name;
			$user->role = $request->role;
			$user->password = $request->password == null ? $user->password : bcrypt($request->password);
			$user->save();
			$response = [
							'message' 	=> 'Device Updated Successfully',
							'code'		=> 200,
							'status' 	=> true,
							'device'	=> $user
						];
		}
        return json_encode($response);
    }
	/**
     * update device
     *
     * @return [json] object
     */
    public function DeleteDevice(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'device_ids' => 'required',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$ids = explode(",", $request->device_ids);
			if(User::whereIn('id',$ids)->delete()){
				$response = [
								'message' 	=> 'Deleted Successfully',
								'code'		=> 200,
								'status' 	=> true
							];
			}
			else {
				$response = [
							'message' 	=> 'Device Not Found',
							'code'		=> 404,
							'status' 	=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * update own password
     *
     * @return [json] object
     */
    public function UserUpdatePassword(Request $request)
    {
        $userLogin = new UserLogin();
        $validator = Validator::make($request->all(),[
            'password' => 'required|string|confirmed',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$userLogin->setMessage($errorMessage);
			$userLogin->setResponseCode(500);
			$userLogin->setStatus(false);
		}else{
			$user = User::find($request->user()->id);
			$user->password = bcrypt($request->password);
			$user->save();
			
			
			$hotel = Hotel::find($user->hotel_id);
			if($hotel != null){
				$userLogin->setFirstLogin($hotel->name != null ? 1 : 0);
				$userLogin->setHotelDetails($hotel->id, $hotel->name, $hotel->email, $hotel->contact_no, $hotel->address, asset($hotel->image), $hotel->hotel_type);
			}
			
			$userLogin->setUserDetails($user->id, $user->name, $user->email, $user->mobile_no, $user->role);
			
			$userLogin->setAccessToken($request->bearerToken());
			$userLogin->setMessage('Updated Successfully');
			$userLogin->setResponseCode(200);
			$userLogin->setStatus(true);
			
		}
        return $userLogin->getJson();
    }
	/**
     * update own password
     *
     * @return [json] object
     */
    public function UserForgetPassword(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$user = User::where('email', $request->email)->first();
			if($user != null){
				$credentials = ['email' => $user->email];
				$response = Password::sendResetLink($credentials, function (Message $message) {
					$message->subject($this->getEmailSubject());
				});
				
				$response = [
					'message'		=> 'We have sent you an Email, Please Check your email',
					'response_code' => 200,
					'status' 		=> true
					
				];
			} else {
				$response = [
					'message' 		=> 'Email not found',
					'response_code' => 404,
					'status' 		=> false
				];
			}
		}
        return json_encode($response);
    }
	/**
     * edit own profile
     *
     * @return [json] object
     */
    public function UserEditProfile(Request $request)
    {
        $userLogin = new UserLogin();
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'mobile_no' => 'string|unique:users',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$userLogin->setMessage($errorMessage);
			$userLogin->setResponseCode(500);
			$userLogin->setStatus(false);
		}else{
			$user = User::find($request->user()->id);
			$user->name = $request->name;
			$user->mobile_no = $request->mobile_no != null ? $request->mobile_no : $user->mobile_no;
			$user->save();
			
			$hotel = Hotel::find($user->hotel_id);
			if($hotel != null){
				$userLogin->setFirstLogin($hotel->name != null ? 1 : 0);
				$userLogin->setHotelDetails($hotel->id, $hotel->name, $hotel->email, $hotel->contact_no, $hotel->address, asset($hotel->image), $hotel->hotel_type);
			}
			
			$userLogin->setUserDetails($user->id, $user->name, $user->email, $user->mobile_no, $user->role);
			
			$userLogin->setAccessToken($request->bearerToken());
			$userLogin->setMessage('Updated Successfully');
			$userLogin->setResponseCode(200);
			$userLogin->setStatus(true);
		}
        return $userLogin->getJson();
    }
	/**
     * edit Hotel Details
     *
     * @return [json] object
     */
    public function UserEditHotel(Request $request)
    {
        $userLogin = new UserLogin();
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|string',
            'contact_no' => 'required|string',
            'address' => 'required|string',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$userLogin->setMessage($errorMessage);
			$userLogin->setResponseCode(500);
			$userLogin->setStatus(false);
		}else{
			$hotel = Hotel::where('email', $request->email)->where('id', '!=', $request->user()->hotel_id)->first();
			if($hotel == null){
				$hotel = Hotel::where('contact_no', $request->contact_no)->where('id', '!=', $request->user()->hotel_id)->first();
				if($hotel == null){
					$hotel = Hotel::find($request->user()->hotel_id);
					$hotelTypeDisable = true;
					if($hotel->name == null && $hotel->email == null && $hotel->contact_no == null){
						$hotelTypeDisable = false;
					}
					$file = $request->file('image');
					if(!$file == null){
						$destinationPath = public_path()."/hotels/";
						$fileName = time()."_".$file->getClientOriginalName();
						$filePathFromPublicDir = "hotels/".$fileName;
						$file->move($destinationPath,$fileName);
						
						$hotel->name = $request->name;
						$hotel->email = $request->email;
						$hotel->contact_no = $request->contact_no;
						$hotel->address = $request->address;
						$hotel->image = $filePathFromPublicDir;
						$hotel->hotel_type = 0;
						$hotel->save();
					}
					else{
						$hotel->name = $request->name;
						$hotel->email = $request->email;
						$hotel->contact_no = $request->contact_no;
						$hotel->address = $request->address;
						$hotel->hotel_type = 0;
						$hotel->save();
					}
					
					if(!$hotelTypeDisable){
						$menu = new FoodType();
						$menu->name = "Pizza";
						$menu->starts_at = "00:00:01";
						$menu->ends_at = "23:59:59";
						$menu->image = "food_types/pizza.jpg";
						$menu->hotel_id = $hotel->id;
						$menu->save();
					}
					
					if($hotel != null){
						$userLogin->setFirstLogin($hotel->name != null ? 1 : 0);
						$userLogin->setHotelDetails($hotel->id, $hotel->name, $hotel->email, $hotel->contact_no, $hotel->address, asset($hotel->image), $hotel->hotel_type);
					}
					
					$user = User::find($request->user()->id);
					
					$userLogin->setUserDetails($user->id, $user->name, $user->email, $user->mobile_no, $user->role);
					
					$userLogin->setAccessToken($request->bearerToken());
					$userLogin->setMessage('Updated Successfully');
					$userLogin->setResponseCode(200);
					$userLogin->setStatus(true);
				}else {
					$userLogin->setMessage('Contact No. already Exist');
					$userLogin->setResponseCode(500);
					$userLogin->setStatus(false);
				}
			}else {
				$userLogin->setMessage('Email already Exist');
				$userLogin->setResponseCode(500);
				$userLogin->setStatus(false);
			}
		}
        return $userLogin->getJson();
    }
	/**
     * Get All Users From Same Hotel
     *
     * @return [json] object
     */
    public function GetUsers(Request $request)
    {
        $response;
		$users = User::where('hotel_id', $request->user()->hotel_id)->where('id','!=',$request->user()->id)->whereIn('role', [3,4])->get();
		if ($users->isEmpty()) {
			$response = [
							'message' 	=> 'Device Not Found.',
							'code'		=> 404,
							'status' 	=> false,
							'users'		=> array()
						];
		}else {
			$usersResponse = array();
			foreach($users as $user){
				array_push($usersResponse, 
					array(
						'id'			=> $user->id,
						'name'			=> $user->name,
						'email'			=> $user->email,
						'role'			=> $user->role,
						'role_name'		=> Config::get("constants.role_names.role".$user->role),
						'device_token'	=> $user->deviceInfo != null ? $user->deviceInfo->device_token : ""
					)
				);
			}
			$response = [
							'message' 	=> 'List Fetched Successfully',
							'code'		=> 200,
							'status' 	=> true,
							'users'		=> $usersResponse
						];
		}
        return json_encode($response);
    }
	/**
     * Get All hotels if role = admin
     *
     * @return [json] object
     */
    public function GetHotels(Request $request)
    {
        $getHotels = new GetHotels();
		if(Auth::user()->role == 1){
			$hotels = Hotel::all();
			if(!$hotels->isEmpty()) {
				$hotelsResponse = array();
				foreach($hotels as $hotel){
					$selected = 0;
					if($hotel->id.'' == Auth::user()->hotel_id.''){
						$selected = 1;
					}
					$owner_name = implode(', ', User::where('hotel_id', $hotel->id)->where('id', '!=', Auth::user()->id)->pluck('name')->toArray());
					$getHotels->addHotelToList($hotel->id, $hotel->name, $hotel->email, $hotel->contact_no, asset($hotel->image), $hotel->address, $owner_name, $selected);
				}
				$getHotels->setMessage('List Fetched Successfully');
				$getHotels->setResponseCode(200);
				$getHotels->setStatus(true);
			}
		}else {
			$getHotels->setMessage('Only Admin have access');
			$getHotels->setResponseCode(500);
			$getHotels->setStatus(false);
		}
        return $getHotels->getJson();
    }
	/**
     * Get All hotels if role = admin
     *
     * @return [json] object
     */
    public function SelectHotel(Request $request)
    {
        $selectHotel = new SelectHotel();
		if(Auth::user()->role == 1){
			$validator = Validator::make($request->all(),[
				'hotel_id' => 'required|integer',
			]);
			if ($validator->fails()) {
				$errorMessage = "";
				$errorArray = json_decode($validator->messages());
				foreach($errorArray as $key => $value) {
					$errorMessage = $errorMessage.$value[0].", ";
				}
				$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
				
				$selectHotel->setMessage($errorMessage);
				$selectHotel->setResponseCode(500);
				$selectHotel->setStatus(false);
			}else{
				$hotel = Hotel::find($request->hotel_id);
				if($hotel){
					Auth::user()->hotel_id = $request->hotel_id;
					Auth::user()->save();

					$selectHotel->setMessage('Hotel Successfully Selected');
					$selectHotel->setResponseCode(200);
					$selectHotel->setStatus(true);
				}
			}
		}else {
			$selectHotel->setMessage('Only Admin have access');
			$selectHotel->setResponseCode(500);
			$selectHotel->setStatus(false);
		}
        return $selectHotel->getJson();
    }
	/**
     * Get All Users From Same Hotel
     *
     * @return [json] object
     */
    public function UserDashboard(Request $request)
    {
        $response = '';
		$validator = Validator::make($request->all(),[
            'from_date' => 'required|string',
            'to_date' => 'required|string',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$foodTypes = FoodType::where('status', 1)->where('hotel_id', $request->user()->hotel_id)->get();
			if ($foodTypes->isEmpty()) {
				$response = [
								'message' 	=> 'List Not Found',
								'code'		=> 404,
								'status' 	=> false,
							];
			}else {
				$fromDate = Carbon::parse($request->from_date);
				$toDate = Carbon::parse($request->to_date);
				$toDate = $toDate->modify('+1 day');
				$totalRevenueToday = 0;
				//$totalRevenue = OrderSummary::where('status', 2)->where('hotel_id', $request->user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->sum('total_price');
				$totalRevenue = OrderSummary::where('status', 2)->where('hotel_id', $request->user()->hotel_id)->sum('total_price');
				//$totalOrders = OrderSummary::where('status', 2)->where('hotel_id', $request->user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->count();
				$totalOrders = OrderSummary::where('status', 2)->where('hotel_id', $request->user()->hotel_id)->count();
				$orderSummaries = OrderSummary::where('status', 2)->where('hotel_id', $request->user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->orderby('id', 'DESC')->get();
				$orderSummariesResponse = array();
				foreach($orderSummaries as $orderSummary){
					$totalRevenueToday += $orderSummary->total_price;
					$orderDetails = $orderSummary->orderDetails;
					$orderDetailsResponse = array();
					$employeeName = "";
					$preparationTime = "";
					foreach($orderDetails as $orderDetail){
						$foodItem = FoodItem::find($orderDetail->item_id);
						$employee = Employee::find($orderDetail->prepared_by);
						
						$startTime = Carbon::parse($orderDetail->prepare_start_time);
						$endTime = Carbon::parse($orderDetail->prepare_end_time);
						
						$preparationTime =  gmdate('H:i:s', $startTime->diffInSeconds($endTime));
						$employeeName = $employee->name ?? "";
						array_push($orderDetailsResponse, array(
														'id'			=> $orderDetail->id,
														'submenu_id'	=> $orderDetail->item_id,
														'submenu_name'	=> $foodItem->name ?? "",
														'submenu_image'	=> asset($foodItem->image ?? ""),
														'item_count'	=> $orderDetail->item_count,
														'item_price'	=> "".$orderDetail->price,
														'total_price'	=> "".$orderDetail->price*$orderDetail->item_count,
														'discount'		=> "".$orderDetail->discount,
														'extra_ingredients'=> $orderDetail->extraIngredients,
														));
					}
					array_push($orderSummariesResponse, array(
								'token_id'			=> $orderSummary->id,
								'token_no'			=> $orderSummary->daily_token,
								'order_no'			=> $orderSummary->order_no,
								'note'				=> $orderSummary->note,
								'order_date'		=> $orderSummary->updated_at->format('Y-m-d h:m:s'),
								'total_price'		=> $orderSummary->total_price,
								'prepared_by'		=> $employeeName,
								'preparation_time'	=> $preparationTime,
								'order_details' 	=> $orderDetailsResponse,
							));
				}
				if($totalOrders){
					$response = [
									'message' 		=> 'List Fetched Successfully',
									'code'			=> 200,
									'status' 		=> true,
									'total_orders'	=> $totalOrders,
									'total_revenue'	=> round($totalRevenue, 2),
									'today_revenue'	=> round($totalRevenueToday, 2),
									'today_orders'	=> $orderSummariesResponse,
								];
				}else{
					$response = [
									'message' 		=> 'List Not Found',
									'code'			=> 404,
									'status' 		=> false,
								];					
				}
			
			}
		}
        return json_encode($response);
    }
	
}