<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\UserDeviceInfo;
use App\Hotel;
class SignUpController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] json
     */
	 
    public function signup(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'mobile_no' => 'required|string|unique:users',
            'password' => 'required|string|confirmed',
			'os_type' => 'required|string',
			'os_version' => 'required|string',
			'device_model' => 'required|string',
			'device_token' => 'required|string',
			'app_version' => 'required|string'
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
						];
		}else{
			$hotel = new hotel();
			$hotel->save();
			$user = new User([
				'name' => $request->name,
				'email' => $request->email,
				'mobile_no' => $request->mobile_no,
				'hotel_id' => $hotel->id,
				'role' => 2,
				'password' => bcrypt($request->password)
			]);
			$user->save();
			
			$userDeviceInfo = new UserDeviceInfo();
			$userDeviceInfo->user_id = $user->id;
			$userDeviceInfo->os_type = $request->os_type;
			$userDeviceInfo->os_version = $request->os_version;
			$userDeviceInfo->device_model = $request->device_model;
			$userDeviceInfo->device_token = $request->device_token;
			$userDeviceInfo->app_version = $request->app_version;
			$userDeviceInfo->save();
			
			$response = [
							'message' 	=> 'Successfully created user!',
							'code'		=> 200,
							'status'	=> true,
							'user' 		=> $user
						];
		}
        return json_encode($response);
    }
	
	/**
     * Create owner
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] json
     */
	 
    public function CreateOwner(Request $request)
    {
		$response;
		if(Auth::user()->role == 1){
			$validator = Validator::make($request->all(),[
				'name' => 'required|string',
				'email' => 'required|string|email|unique:users',
				'mobile_no' => 'required|string|unique:users',
				'password' => 'required|string|confirmed',
			]);
			if ($validator->fails()) {
				$errorMessage = "";
				$errorArray = json_decode($validator->messages());
				foreach($errorArray as $key => $value) {
					$errorMessage = $errorMessage.$value[0].", ";
				}
				$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
				$response = [
								'message' 	=> $errorMessage,
								'code'		=> 500,
								'status' 	=> false,
							];
			}else{
				$hotel = new hotel();
				$hotel->save();
				$user = new User([
					'name' => $request->name,
					'email' => $request->email,
					'mobile_no' => $request->mobile_no,
					'hotel_id' => $hotel->id,
					'role' => 2,
					'password' => bcrypt($request->password)
				]);
				$user->save();
				Auth::user()->hotel_id = $hotel->id;
				Auth::user()->save();
				
				$response = [
								'message' 	=> 'Owner Successfully created',
								'code'		=> 200,
								'status'	=> true,
								'user' 		=> $user
							];
			}
		}else {
			$response = [
								'message' 	=> 'Only Admin have access',
								'code'		=> 500,
								'status' 	=> false,
							];
		}
        return json_encode($response);
    }
}