<?php

namespace App\Api\Models;

class GetHotels
{
	private $_responseCode = 404;
	private $_status = false;
	private $_message = 'List Not Found.';
	private $_hotels = array();
	
	public function getJson(){
		return json_encode(array(
			'response_code'	=> $this->_responseCode,
			'status'		=> $this->_status,
			'message'		=> $this->_message,
			'hotels'		=> $this->_hotels,
		));
	}
	
	public function setResponseCode($responseCode){
		$this->_responseCode = $responseCode;
	}
	
	public function setStatus($status){
		$this->_status = $status;
	}
	
	public function setMessage($message){
		$this->_message = $message;
	}
	
	public function addHotelToList($id, $name, $email, $contactNo, $image, $address, $ownerName, $selected){
		array_push($this->_hotels, array(
						'id'			=> $id,
						'name'			=> $name.'',
						'email'			=> $email.'',
						'contact_no'	=> $contactNo.'',
						'image'			=> $image.'',
						'address'		=> $address.'',
						'owner_name'	=> $ownerName.'',
						'selected'		=> $selected,
					));
	}
}
