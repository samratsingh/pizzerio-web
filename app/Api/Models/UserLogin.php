<?php

namespace App\Api\Models;

class UserLogin
{
	private $_responseCode = 401;
	private $_status = false;
	private $_message = 'Unauthenticated';
	private $_accessToken = '';
	private $_firstLogin = 0;
	private $_user = array();
	private $_hotel = array();
	
	
	public function __construct(){
		$this->_hotel = array('id'			=> 0,
							'name'			=> "",
							'email' 		=> "",
							'contact_no'	=> "",
							'address'		=> "",
							'image'			=> "",
							'hotel_type'	=> "",
							);
							
		$this->_user = array(
						'id'			=> 0,
						'name'			=> "",
						'email' 		=> "",
						'mobile_no'		=> "",
						'role'			=> 0,
					);
	}
	
	public function getJson(){
		$this->_user['hotel'] = $this->_hotel;
		return json_encode(array(
			'response_code'	=> $this->_responseCode,
			'status'		=> $this->_status,
			'message'		=> $this->_message,
			'access_token'	=> $this->_accessToken,
			'first_login'	=> $this->_firstLogin,
			'user'			=> $this->_user,
		));
	}
	
	public function setResponseCode($responseCode){
		$this->_responseCode = $responseCode;
	}
	
	public function setStatus($status){
		$this->_status = $status;
	}
	
	public function setMessage($message){
		$this->_message = $message;
	}
	
	public function setFirstLogin($firstLogin){
		$this->_firstLogin = $firstLogin;
	}
	
	public function setAccessToken($accessToken){
		$this->_accessToken = $accessToken;
	}
	
	public function setHotelDetails($id, $name, $email, $contactNo, $address, $image, $hotelType){
		$this->_hotel = array('id'			=> $id+0,
							'name'			=> $name."",
							'email' 		=> $email."",
							'contact_no'	=> $contactNo."",
							'address'		=> $address."",
							'image'			=> $image."",
							'hotel_type'	=> $hotelType."",
							);
	}
	
	public function setUserDetails($id, $name, $email, $mobileNo, $role){
		$this->_user = array(
						'id'			=> $id,
						'name'			=> $name."",
						'email' 		=> $email."",
						'mobile_no'		=> $mobileNo."",
						'role'			=> $role,
					);
	}
}
