<?php

namespace App\Api\Models;

class GetMenus
{
	private $_responseCode = 404;
	private $_status = false;
	private $_message = 'Menu Not Found.';
	private $_menus = array();
	private $_hotel = array();
	
	public function getJson(){
		return json_encode(array(
			'code'		=> $this->_responseCode,
			'status'	=> $this->_status,
			'message'	=> $this->_message,
			'menus'		=> $this->_menus,
			'hotel'		=> $this->_hotel,
		));
	}
	
	public function setResponseCode($responseCode){
		$this->_responseCode = $responseCode;
	}
	
	public function setStatus($status){
		$this->_status = $status;
	}
	
	public function setMessage($message){
		$this->_message = $message;
	}
	
	public function addMenuToList($id, $name, $image, $startsAt, $endsAt, $totalItems){
		array_push($this->_menus, array(
						'id'			=> $id,
						'name'			=> $name.'',
						'image'			=> $image.'',
						'starts_at'		=> $startsAt.'',
						'ends_at'		=> $endsAt.'',
						'total_items'	=> $totalItems,
					));
	}
	
	public function setHotelDetails($id, $name, $email, $contactNo, $address, $image){
		$this->_hotel = array(
						'id'			=> $id,
						'name'			=> $name."",
						'email' 		=> $email."",
						'contact_no'	=> $contactNo."",
						'address'		=> $address."",
						'image'			=> $image."",
					);
	}
}
