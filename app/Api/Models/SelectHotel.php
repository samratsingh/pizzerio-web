<?php

namespace App\Api\Models;

class SelectHotel
{
	private $_responseCode = 404;
	private $_status = false;
	private $_message = 'Hotel Not Found.';
	
	public function getJson(){
		return json_encode(array(
			'response_code'	=> $this->_responseCode,
			'status'		=> $this->_status,
			'message'		=> $this->_message,
		));
	}
	
	public function setResponseCode($responseCode){
		$this->_responseCode = $responseCode;
	}
	
	public function setStatus($status){
		$this->_status = $status;
	}
	
	public function setMessage($message){
		$this->_message = $message;
	}
	
}
