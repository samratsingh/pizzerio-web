<?php

namespace App;

class FirebaseNotification
{
	
	public static function instance()
	{
		return new FirebaseNotification();
	}
	
    public function SendNotification($title, $message, $tokensArray) {

		$url = 'https://fcm.googleapis.com/fcm/send';

		$fields = array (
				'priority', 10,
				'registration_ids' => $tokensArray,
				'notification' => array (
						'title' => $title,
						'body'	=> $message,
						'sound'	=> 'Default',
				),
		);

		$headers = array (
				'Authorization: key=' . 'AAAAJeJwsOY:APA91bE8fS9ChLO63-AtQ9h1jjH9UaIc0xJAYAOKuVZkLUJGoITrG7I9-Dg5dgQMN8tGocFGS3PooyvKTxZOGmnCApkUNB2u4tTzXrakwz058LkVs_SrqiBZi9rQuAqXdhGZWx7LV1tf',
				'Content-Type: application/json'
		);

		$ch = curl_init(); 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		
		return $result;
	}
}
