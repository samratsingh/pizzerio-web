<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FoodItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('image');
			$table->decimal('price', 30, 2);
			$table->decimal('discount', 30, 2)->default(0)->comment('should be in percentage');
			$table->integer('status')->default(1)->comment('1 -> active , 0 -> inactive');
			$table->integer('veg_status')->default(1)->comment('1 -> veg , 0 -> non-veg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_items');
    }
}
