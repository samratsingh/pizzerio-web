<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderSummarys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_summarys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daily_token')->default(0);
            $table->integer('user_id');
            $table->decimal('total_price', 30, 2)->nullable();
            $table->decimal('total_discount', 30, 2)->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_summarys');
    }
}
