<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserDeviceInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_device_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('os_type');
            $table->string('os_version');
            $table->string('device_model');
            $table->string('device_token');
            $table->string('app_version');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_device_info');
    }
}
