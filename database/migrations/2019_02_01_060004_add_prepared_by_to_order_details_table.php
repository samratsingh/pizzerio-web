<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreparedByToOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_details', function (Blueprint $table) {
            $table->timestamp('prepare_end_time')->useCurrent()->after('summary_id');
            $table->timestamp('prepare_start_time')->useCurrent()->after('summary_id');
            $table->integer('prepared_by')->default(0)->after('summary_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details', function (Blueprint $table) {
            $table->dropColumn('prepare_end_time');
            $table->dropColumn('prepare_start_time');
            $table->dropColumn('prepared_by');
        });
    }
}
