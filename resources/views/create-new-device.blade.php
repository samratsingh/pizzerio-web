@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Add Device </h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-new-device') }}" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="device-name">Device Name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="device_name" type="text" id="device-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="device-password">Password <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="device_password" type="password" id="device-password" pattern=".{6,12}" title="6 to 12 characters" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="device-role">Select Role<span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="device_role" type="text" id="device-role" required="required" class="form-control col-md-7 col-xs-12">
					<option value=''>Select Device Role</option>
					<option value="3">Kitchen</option>
					<option value="4">Table</option>
				</select>
			  </div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	});
  </script>



  </div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
