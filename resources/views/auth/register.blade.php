
<!-- for disable register page .. redirect to login-->
<script>
window.location = "{{route('login')}}";
</script>
<!-- delete for access page -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ config('app.name', 'Laravel') }} </title>

  <!-- Bootstrap core CSS -->

  <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">

  <link href="{{ asset('admin/fonts/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/animate.min.css') }}" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/icheck/flat/green.css') }}" rel="stylesheet">

  <script src="{{ asset('admin/js/jquery.min.js') }}"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

	<!-- Styles -->
	<style>
		html, body {
			background-color: #F7F7F7;
			color: #636b6f;
			font-family: 'Nunito', sans-serif;
			font-weight: 200;
			height: 100vh;
			margin: 0;
		}

		.full-height {
			height: 100vh;
		}

		.flex-center {
			align-items: center;
			display: flex;
			justify-content: center;
		}

		.position-ref {
			position: relative;
		}

		.top-right {
			position: absolute;
			right: 10px;
			top: 18px;
		}

		.content {
			text-align: center;
		}

		.title {
			font-size: 84px;
		}

		.links > a {
			color: #636b6f;
			padding: 0 25px;
			font-size: 12px;
			font-weight: 600;
			letter-spacing: .1rem;
			text-decoration: none;
			text-transform: uppercase;
		}

		.m-b-md {
			margin-bottom: 30px;
		}
		
		.bold {
			color:#73879C;
			font-weight:bold;
		}
	</style>
</head>

<body style="background:#F7F7F7;">

  <div class="top-right links">
  </div>

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>
    <div id="wrapper">
      <div id="login" class="animate form">
        <section class="login_content">
          
          <form method="POST" id="register-form" action="{{ route('register') }}">
			@csrf
            <h1>Create Account</h1>
            <div>
				<input id="name" type="text" pattern="[a-zA-Z\s]*" title="Please enter alphabets only." class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

				@if ($errors->has('name'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
				@endif
            </div>
            <div>
				<input id="email" type="email" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter valid Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>

				@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
            </div>
			<div>
				<input id="mobile_no" type="text" class="form-control{{ $errors->has('mobile_no') ? ' is-invalid' : '' }}" name="mobile_no" value="{{ old('mobile_no') }}" placeholder="Mobile No" required pattern="[0-9]{6,15}" title="Please Enter Valid Mobile No.">

				@if ($errors->has('mobile_no'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('mobile_no') }}</strong>
					</span>
				@endif
            </div>
            <div>
				<input id="password1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" pattern=".{6,}" title="Six or more characters" placeholder="Password" required>

				@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
				@endif
            </div>
            <div>
				<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
            </div>
            <div>
                <button type="submit" class="btn btn-default submit bold" style="margin-left:4px">Sign Up</button>
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link bold">Already a member ?
                <a href="{{ route('login') }}" class="to_register bold"> Log in </a>
              </p>
              <div class="clearfix"></div>
              <br />
              <div>
                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> {{ config('app.name', 'Laravel') }}</h1>

                <p class="bold">{{ config('app.copyright_text', 'Hotel Management System') }}</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
	  
	  
    </div>
  </div>
</body>
<script>
$( document ).ready(function(){
	function validatePassword(){
		if($("#password1").val() != $("#password-confirm").val()) {
			$("#password-confirm").get(0).setCustomValidity("Passwords Don't Match");
		} else {
			$("#password-confirm").get(0).setCustomValidity("");
		}
	}

	$("#password1").change(validatePassword);
	$("#password-confirm").keyup(validatePassword);
});
</script>
</html>
