@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Owner Profile</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">

		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th style="width: 20%">Full Name</th>
				<th>Email</th>
				<th>Phone No.</th>
				<th>Registered On</th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<!--<td>#</td>-->
				<td>
					{{ $user->name }}
				</td>
				<td>
					{{ $user->email ?? "Unavailable" }}
				</td>
				<td>
					{{ $user->mobile_no ?? "Unavailable"  }}
				</td>
				<td>
					{{ $user->created_at->diffForHumans() }}
				</td>	
				<td>
					<a href="{{ route('edit-owner-profile') }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
				</td>	
				
			  </tr>
			</tbody>
		  </table>

		  <!-- end project list -->

		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
