@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Add Employee</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-add-employee') }}" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
		  
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employee-name">Name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="name" type="text" id="employee-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<!--<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Password </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input id="password1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" pattern=".{6,}" title="Six or more characters" placeholder="Password">
			  </div>
			</div>
			<div class="form-group">
			  <label for="contact-no" class="control-label col-md-3 col-sm-3 col-xs-12">Confim Password </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
			  </div>
			</div>-->
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="email" type="email" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter valid Email" id="email" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label for="contact-no" class="control-label col-md-3 col-sm-3 col-xs-12">Mobile No <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="mobile_no" type="tel" id="mobile-no" required="required" pattern="[0-9]{6,15}" title="Please Enter Valid Mobile No." class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label for="contact-no" class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="radio" class="flat" name="gender" id="genderM" value="Male" checked=""  required="required" data-parsley-multiple="gender" data-parsley-id="3341" style="position: absolute; opacity: 0;"><span style="color: #73879C !important"> Male </span><br><input type="radio" class="flat" name="gender" id="genderF" value="Female" data-parsley-multiple="gender" data-parsley-id="3341" style="position: absolute; opacity: 0;"><span style="color: #73879C !important"> Female </span>
			  </div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	});
  </script>



  </div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
