@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Update Profile</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-owner-profile') }}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="owner-name">Owner Name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="name" pattern="[a-zA-Z\s]*" title="Please enter alphabets only." type="text" value="{{ $user->name }}" id="owner-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="email" type="email" value="{{ $user->email }}" id="email" required="required" class="form-control col-md-7 col-xs-12" readonly>
			  </div>
			</div>
			<div class="form-group">
			  <label for="contact-no" class="control-label col-md-3 col-sm-3 col-xs-12">Contact No <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="contact_no" type="tel" value="{{ $user->mobile_no }}" id="contact-no" class="form-control col-md-7 col-xs-12" readonly>
			  </div>
			</div>
			<!--div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<textarea name="address" class="form-control col-md-7 col-xs-12">{{ $user->address }}</textarea>
			  </div>
			</div-->
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	});
  </script>



  </div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
