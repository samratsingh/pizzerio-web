@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Manage Food Items</h2>
		  <ul class="navbar-right">
			<a href="{{ route('add-food-item') }}" class="btn btn-info btn-xs">Add Food Item</a>
		  </ul>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		@if(!$food_items->isEmpty())
		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th style="width: 12%">Image</th>
				<th style="width: 12%">Name</th>
				<th style="width: 8%">Price</th>
				<!--<th style="width: 8%">Type</th>-->
				<th style="width: 13%">Category</th>
				<th style="width: 14%">Extra(s)</th>
				<th style="width: 22%">Action</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($food_items as $food_item)
			  <tr>
				<!--<td>#</td>-->
				<td>
					<img src="{{ asset($food_item->image) }}" style="border-radius: 50%;" height=70 width=70>
				</td>
				<td>
					{{ $food_item->name ?? "Unavailable" }}
				</td>
				<td>
					{{ $food_item->price }}
				</td>
				<!--<td>
					@if($food_item->veg_status) Veg @else Non-Veg @endif
				</td>-->
				<td>
				@if($food_item->foodCategories->count() > 0)
					{{ implode(', ', $food_item->foodCategories->pluck('name')->toArray()) }}
				@else
					Category Not Found
				@endif
				</td>
				<td>
				@if($food_item->foodIngredients->count() > 0)
					{{ implode(', ', $food_item->foodIngredients->pluck('name')->toArray()) }}
				@else
					Extra's Not Found
				@endif
				</td>
				<td>
					@if($food_item->status)
						<a 
							href="{{ route('change-food-item-status') }}"
							onclick="event.preventDefault(); submitChangeFoodItemStatusForm({{ $food_item->id }}, 0)"
							class="btn btn-danger btn-xs" style="width:85px">
							<i class="fa fa-ban"></i> UnPublish 
						</a>
					@else

						<a 
							href="{{ route('change-food-item-status') }}"
							onclick="event.preventDefault(); submitChangeFoodItemStatusForm({{ $food_item->id }}, 1)"
							class="btn btn-publish btn-xs" style="width:85px">
							<i class="fa fa-check"></i> Publish 
						</a>
					@endif
				
				  <a href="{{ route('edit-food-item', $food_item->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
				  <a href="{{ route('delete-food-item', $food_item->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
				</td>
			  </tr>
			@endforeach
			</tbody>
			
		  <form id="change-food-item-status-form" action="{{ route('change-food-item-status') }}"   method="POST" style="display: none;">
			{{ csrf_field() }}
			<input type="hidden" name="food_item_id" id="food-item-id">
			<input type="hidden" name="status" id="status">
          </form>
			
		  </table>
		  <!-- end project list -->
		@else
			<div class="" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2>List Not Found</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		@endif
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeFoodItemStatusForm(food_item_id, status) 
		{
			$("#food-item-id").val(food_item_id);
			$("#status").val(status);
			$("#change-food-item-status-form").submit();
		}
	</script>
@endsection
